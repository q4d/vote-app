from flask import Flask, render_template, request, redirect, make_response
from flask_bootstrap import Bootstrap

import datetime
import uuid

app = Flask(__name__, static_url_path='/static')
Bootstrap(app)

poll_data = {
    'question': 'How many stars would you rate this session?',
    'fields': ['1', '2', '3', '4', '5']
}

filename = 'data.txt'

cookie_id = str(uuid.uuid4())

@app.route('/')
def root():
    return render_template('poll.html', data=poll_data)


def getcookie():
    name = request.cookies.get(cookie_id)
    if name:
        return True
    else:
        return False


@app.route('/poll')
def poll():

    vote = request.args.get('field')
    if not getcookie():
        out = open(filename, 'a')
        out.write(vote + '\n')
        out.close()
        resp = make_response(redirect('/thankyou'))
        resp.set_cookie(cookie_id, "banaan", expires=datetime.datetime.now()
                        + datetime.timedelta(days=30))
        return resp
    else:
        return redirect("/youalreadyvoted")


@app.route('/thankyou')
def thankyou():
    return render_template('thankyou.html', data=poll_data)


@app.route('/youalreadyvoted')
def alreadyvoted():
    return render_template('alreadyvoted.html', data=poll_data)


@app.route('/results')
def show_results():
    votes = {}
    scores = 0
    counter = 0
    for f in poll_data['fields']:
        votes[f] = 0

    f = open(filename, 'r')
    for line in f:
        List = line.rstrip("\n")
        List2 = List.split(",")
        vote = List2.pop()
        scores += int(vote)
        votes[vote] += 1
        counter += 1

    average = scores / counter

    return render_template('results.html', data=poll_data, votes=votes,
                           average=round(average, 2), counter=counter,
                           total=scores)


if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0')
