# vote-app

## Build and run the container

```
docker run --rm -p 5000:5000 $(docker build . -q)
```

This exposes port 5000 (`5000:`).

You can start a few containers:

```
docker run --rm -p 5000:5000 $(docker build . -q)
docker run --rm -p 5000:5001 $(docker build . -q)
docker run --rm -p 5000:5002 $(docker build . -q)
```